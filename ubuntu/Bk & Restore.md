# BK & Restore do DHIS2

### Backups
Uma vez que a base de dados das instâncias são armazenadas no container postgres, para que se faça bk é necessário que os comandos sejam executandos a nível do container postgres os comandos de bk a nível do SGBD postgres:

```sh
sudo lxc exec postgres -- pg_dump -O -T 'analytics_*' -Fp <nome-instância> | gzip > <nome-ficheiro>.sql.gz
```
A opção `-T 'analytics_*'` permite ignorar as tabelas analíticas, permitindo que o ficheiro seja mais compacto, assumindo um tamanho menor.


### Restore 

O seguinte comando é usado para restauração da dase de dados a níveld o SGBD postgreSQL:
Antes de tudo é importante parar a instância e activar depois do restore.

```sh
sudo dhis2-restoredb <nome-ficheiro>.sql.gz <nome-instância>
```

