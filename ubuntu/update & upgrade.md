# Update e Upgrade do DHIS2

### Update

Apenas o novo deploy: 

`sudo dhis2-deploy-war -l [link-da-nova-versao] [nome-container]`


### Upgrade

1. Procurar na documentação se é necessáio executar algum script/query (https://dhis2.org/downloads/archive/)

2. Caso seja necessário executar alguma query na base de dados, fazer bk da base de dados da instância

3. `sudo lxc exec postgres bash`

4. `psql <nome-container>`

> *Excução dos scripts*

5. Sair: executanto os comandos na seguinte sequência: `exit`, `\q` e `exit`

6. Findo o processo de execução do script e lembrando que é importante ter um backup, procedemos novamente com o deploy do .war contendo a versão posterior

`sudo dhis2-deploy-war -l [link-da-nova-versao] [nome-container]`

