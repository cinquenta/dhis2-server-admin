# Instalação
Este capítulo contém informações sobre como instalar o DHIS2 em contextos como servidor central online e rede local offline.

## Introdução
DHIS2 é executado em todas as plataformas para as quais existe um Java JDK. DHIS2 é executado no sistema de banco de dados PostgreSQL. DHIS2 é empacotado como um `Java Web Archive` padrão (arquivo WAR) e, portanto, é executado em qualquer container de Servlet, como Tomcat e Jetty.

Recomenda-se a versão 20.04 LTS do Linux Ubuntu, Sistema de Gestão de Base de Dados PostgreSQL e Tomcat como o ambiente para configurações.

O pacote `dhis2-tools` Ubuntu automatiza muitas das tarefas descritas neste guia e é recomendado para a maioria dos utilizadores, especialmente aqueles que não estão familiarizados com a linha de comando ou administração de servidores.


## Especificações do servidor
DHIS2 é um aplicativo intensivo de base de dados e requer que o servidor tenha quantidade suficiente de memória RAM, número de núcleos de CPU e um disco rápido (leitura):

- RAM: pelo menos 2 GB para uma instância pequena, 12 GB para uma instância média, 64 GB ou mais para uma instância grande.
- CPU: 4 núcleos de CPU para uma instância pequena, 8 núcleos de CPU ou mais para uma instância média ou grande.
- Disco: SSD é recomendado como dispositivo de armazenamento. A velocidade mínima de leitura deve ser pelo menos 200 Mb/s. Em termos de espaço em disco, pelo menos 100 GB é recomendado.



## Requisitos de software:
Os seguintes softwares são necessários para o funcionamento do DHIS2:

- Um Sistema Operativo para o qual existe um Java JDK ou JRE versão 8 ou 11. Linux Ubuntu é o recomendado.
- Java JDK. OpenJDK is recommended.
- SGBD PostgreSQL versão 9.6 ou posterior.
	- Extensão de base de dados PostGIS versão 2.2 ou posterior
- Tomcat versão 8.5.50 ou posterior



## Configuração do servidor
Esta secção descreve como configurar uma instância de servidor de DHIS2 no Ubuntu 20.04 de 64 bits com PostgreSQL como SGBD e Tomcat como container Servlet.

### Criação de um utilizador para executar o DHIS2

Antes de tudo, é necessário criar um utilizador dedicado para execução do DHIS2.

Criação de um utilizador denominado `dhis`, executando o comando:

```sh
sudo useradd -d /home/dhis -m dhis -s /bin/false
```

Em seguida, para definir a senha da conta criada:

```sh
sudo passwd dhis
```

É importante definir uma senha forte com pelo menos 15 caracteres aleatórios.


### Criação do diretório de configuração
Criação de um diretório para os arquivos de configuração DHIS2. Este diretório também será usado para aplicativos, arquivos e ficheiros log:

```sh
sudo mkdir /home/dhis/config
sudo chown dhis:dhis /home/dhis/config
```

DHIS2 irá procurar por uma variável de ambiente chamada *DHIS2\_HOME* para localizar o diretório de configuração DHIS2. Este diretório será designado *DHIS2\_HOME* neste guia. Definiremos a variável de ambiente em uma etapa posterior do processo de instalação.

### Definição do fuso horário e local do servidor
Pode ser necessário reconfigurar o fuso horário do servidor para coincidir com o fuso horário do local que o servidor DHIS2 estará cobrindo


```sh
sudo dpkg-reconfigure tzdata
```

Para verificar os locais existentes e instalar novos:

```sh
locale -a
sudo locale-gen nb_NO.UTF-8
```

### Instalação do PostgreSQL

Para instalação do PostgreSQL, é suficiente a execução do seguinte comando:

```sh
sudo apt-get install postgresql-12 postgresql-12-postgis-3
```

Precedido pela criação de um utilizador com privilégios restritos:

```sh
sudo -u postgres createuser -SDRP dhis
```

Digite uma senha segura no prompt, conforme a solicitação.

A base de dados a ser utilizada deverá ser criada executando


```sh
sudo -u postgres createdb -O dhis dhis2
```

executando `exit`, será levado de volta à sessão.

Agora você tem um usuário PostgreSQL chamado *dhis* e um banco de dados chamado *dhis2*.

A extensão PostGIS é necessária para que vários recursos de GIS/mapas funcionem. 

```sh
sudo -u postgres psql -c "create extension postgis;" dhis2
```

Saia do console e volte ao ambiente anterior com *\\q* seguido com *exit*.



### Instalação do JAVA

O Java JDK recomendado para DHIS 2 é OpenJDK 11 (para a versão 2.35 e posterior). Instale com o seguinte comando:

```sh
sudo apt-get install openjdk-11-jdk
```

Para certificar se a instalação foi bem sucedida, utilize o seguinte comando:

```sh
java -version
```


## Configurações DHIS

As informações de conexão da base de dados são fornecidas ao DHIS2 por meio de um arquivo de configuração denominado `dhis.conf`. Crie este arquivo e salve-o no diretório DHIS2\_HOME:

```sh
sudo nano /home/dhis/config/dhis.conf
```

Um arquivo de configuração para PostgreSQL correspondente à configuração acima tem estas propriedades:


```properties
# ----------------------------------------------------------------------
# Database connection
# ----------------------------------------------------------------------

# JDBC driver class
connection.driver_class = org.postgresql.Driver

# Database connection URL
connection.url = jdbc:postgresql:dhis2

# Database username
connection.username = dhis

# Database password
connection.password = xxxx
		
# ----------------------------------------------------------------------
# Server
# ----------------------------------------------------------------------

# Enable secure settings if deployed on HTTPS, default 'off', can be 'on'
# server.https = on

# Server base URL
# server.base.url = https://server.com/
```


### Instalação do Tomcat e DHIS2

Para instalar o container servlet Tomcat, utilizaremos o pacote do utilizador Tomcat executando o seguinte comando:

```sh
sudo apt-get install tomcat9-user
```

Este pacote nos permite criar facilmente uma nova instância do Tomcat. A instância será criada no diretório atual. Um local apropriado é o diretório inicial do usuário `dhis`:

```sh
cd /home/dhis/
sudo tomcat9-instance-create tomcat-dhis
sudo chown -R dhis:dhis tomcat-dhis/
```

Isso criará uma instância em um diretório chamado `tomcat-dhis`. Observe que o pacote `tomcat9-user` permite a criação de qualquer número de instâncias DHIS2, se desejado.

Em seguida, edite o ficheiro `tomcat-dhis/bin/setenv.sh` e adicione as linhas abaixo no final do ficheiro.

```sh
sudo nano tomcat-dhis/bin/setenv.sh
```

```sh
export JAVA_HOME='/usr/lib/jvm/java-11-openjdk-amd64/'
export JAVA_OPTS='-Xms4000m -Xmx7000m'
export DHIS2_HOME='/home/dhis/config'
```

Onde:

* `JAVA_HOME` define a localização da instalação do JDK.
* `JAVA_OPTS` passa parâmetros para o JVM.
     * `-Xms` define a alocação inicial de memória para o espaço de memória heap Java.
     * `-Xmx` define a alocação máxima de memória para o espaço de memória heap Java.
* `DHIS2_HOME` define a localização do arquivo de configuração `dhis.conf` para DHIS2.

Verifique se o caminho dos binários Java está correto, pois eles podem variar de SO para SO, por exemplo, em sistemas AMD, você pode ver`/java-11-openjdk-amd64`. Deve se ajustar esses valores ao seu ambiente.


O arquivo de configuração do Tomcat está localizado em
`tomcat-dhis/conf/server.xml`. O elemento que define a conexão ao DHIS é o elemento *Connector* com a porta 8080. Você pode alterar o número da porta no elemento Conector para uma porta desejada, se necessário.
O atributo `RelaxedQueryChars` é necessário para permitir certos caracteres em URLs usados pelo front-end DHIS2.

```sh
sudo nano tomcat-dhis/conf/server.xml
```

```xml
<Connector port="8080" protocol="HTTP/1.1"
  connectionTimeout="20000"
  redirectPort="8443"
  relaxedQueryChars="[]" />
```


A próxima etapa é baixar o arquivo DHIS2 WAR e colocá-lo no diretório _webapps_ do Tomcat. O ficheiro DHIS2 WAR pode ser baixado a partir do seguinte local:

- https://releases.dhis2.org/

Para baixar o ficheiro, utilize a seguinte sintaxe:
```sh
sudo wget <link-da-versão>
```

Agora mova o ficheiro WAR para o diretório `webapps` do Tomcat. O novo nome do ficheiro WAR será `ROOT.war` para torná-lo disponível em `localhost` diretamente, sem um caminho:

```sh
mv <nome-ficheiro-baixado>.war tomcat-dhis/webapps/ROOT.war
```

DHIS2 nunca deve ser executado como um usuário privilegiado. Depois de modificar o arquivo `setenv.sh`, modifique o script de inicialização para verificar e verificar se o script não foi executado como root.

```sh
sudo nano tomcat-dhis/bin/startup.sh
```


```sh
#!/bin/sh
set -e

if [ "$(id -u)" -eq "0" ]; then
  echo "This script must NOT be run as root" 1>&2
  exit 1
fi

export CATALINA_BASE="/home/dhis/tomcat-dhis"
/usr/share/tomcat9/bin/startup.sh
echo "Tomcat started"
```

### Execução do DHIS2

DHIS2 pode ser iniciado executando:

```sh
sudo -u dhis tomcat-dhis/bin/startup.sh
```

DHIS2 pode ser interrompido executando:

```sh
sudo -u dhis tomcat-dhis/bin/shutdown.sh
```

Para monitoraramento do comportamento do Tomcat, o log é a principal fonte de informações. O registro pode ser visualizado com o seguinte comando:

```sh
 sudo tail -f tomcat-dhis/logs/catalina.out
```

Supondo que o ficheiro .WAR seja denominado ROOT.war, podemos agora aceder sua instância DHIS2 no seguinte URL:


    http://localhost:8080

   ou ainda:

    http://<ip-do-servidor>:8080