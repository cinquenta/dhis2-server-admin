# Containers LXD

## Instalção e configuração 
Para instalar o LXD, execute o seguinte comando:

```sh
sudo snap install lxd
```

Antes de criar qualquer instância, é necessário configurar o LXD.

O seguinte comando serve para iniciar o processo de configuração interativa:

```sh
sudo lxd init
```

Feita a instalação, para verificar os containers disponíveis, utilize o comando:

```sh
sudo lxc list
```

Para criar container, a seguinte fórmula é utilizada: `lxc launch imageserver:imagename instancename`
Nos comandos acima, substitua:
* imageserver - o nome de um servidor de imagem integrado ou adicionado (por exemplo, ubuntu);
* imagename - o nome de uma imagem (por exemplo, 20.04 ou debian / 11).
* instancename - o nome da instância a ser criada, a sua escolha.

## Criação de instâncias
```sh
sudo lxc launch ubuntu:20.04 <nome-container>
```

## Gestão das instâncias

Para listar todas imagens disponíveis
```sh
sudo lxc list
```


#### Start e Stop

Start da instância

```sh
lxc start nome-container
```

Stop da instância
```sh
lxc stop nome-container
```

Entrar na instância

```sh
sudo lxc exec <nome-container> -- /bin/bash
```

Por padrão, será logado como root:

Para sair do container, execute:

```sh
exit
```

#### Executar comandos a partir do host

```sh
lxc exec nome-container -- apt-get update
```




#### Copiar ficheiros e directórios do host para o container

##### Da instância para o host
Ficheiro
```sh
lxc file pull nome-container/caminho-no-container caminho-no-host
```

Directório
```sh
lxc file pull -r nome-container/caminho-no-container caminho-no-host
```

Por exemplo: `lxc file pull instancename/etc/hosts .`

##### Do host para a instância

Ficheiro
```sh
lxc file push caminho-no-host nome-container/caminho-no-container
```
Directório
```sh
lxc file push -r caminho-no-host nome-container/caminho-no-container
```



### Remover a instância
```sh
lxc delete nome-container
```





## Referências
- https://linuxcontainers.org/lxd/getting-started-cli/