## Criação de utilizador
```sh
adduser <username>
```

Atribuindo ao utilizador os pivilégios de super utilizador
```sh
usermod -aG sudo <username>
```

Alterando o utilizador
```sh
su <username>
cd
mkdir .ssh
```

Entrar para o seguinte ficheiro e adicionar a chave pública
```sh
sudo nano .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
exit
```


## Conexão via SSH

```sh
sudo apt-get update
```

```sh
sudo apt-get upgrade
```

```sh
sudo apt-get update
```

instalação do openssh-server

```sh
sudo apt install openssh-server
```

veririfcar o status do ssh

```sh
sudo systemctl status ssh
```

habilitar o firewall ufw 

```sh
sudo ufw enable
```

Permitir a conexão ssh no ufw 

```sh
sudo ufw allow ssh
```

