# dhis2-tools-ng

Ferramentas para configurar dhis2 no lxd. Um guia de instalação simples

DHIS2 é um sistema bastante complexo que requer alguma experiência em Linux, sistemas baseados na web, gestão de bases de dados, etc.

## Pré-requisitos antes de começar:

1. O fuso horário. Para ver a lista de fusos horários disponíveis, digite `timedatectl list-timezones`.

2. Certifique-se de que o seu SO esteja minimamente configurado e com segurança, com o firewall ufw habilitado (não se esqueça de habilitar a porta ssh).

## Processo de instalação

1. Primeiro é necessário ter o script de instalação em sua máquina: `git clone https://github.com/bobjolliffe/dhis2-tools-ng.git`

2. `cd dhis2-tools-ng/setup`

3. `cp configs/containers.json.sample configs/containers.json`

4. Edite as primeiras 3 linhas de configs/containers.json adicionando o seu fqdn, e-mail e time-zone:

5. Executar `sudo ./lxd_setup.sh`

6. Instalação do serviço de scripts, executando `sudo ./install_scripts.sh`

Neste ponto, o proxy, base de dados e monitor devem estar ativos e funcionando. Para certificar, digite `sudo lxc list`:

Agora é possível aeder à página default do apache a partir do navegador `http://<seu-domínio>` ou `http://<seu-ip>`.

## Configuração do proxy

Para casos em que não tenhamos o FQDN associado à máquina, as seguintes configurações serão necessárias para que possamos aceder às nossas instâncias:

1. Configuração do certificado
- Aceder ao container `sudo lxc exec proxy bash` e executar a seguinte sequência de comandos:

```sh
sudo apt-get update
sudo apt-get install openssl
sudo mkdir  /etc/apache2/ssl
```
Gerando o certificado:
```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/dhis.key -out /etc/apache2/ssl/dhis.crt
```

2. Criação do ficheiros mediantes as seguintes passos:

Ainda dentro do container `proxy`:

Mover para o directórios dos ficheiros e criar o ficheiro localhost.conf:

```sh
sudo nano /etc/apache2/sites-available/localhost.conf
```
Copiar e colar no ficheiro o seguinte conteúdo:

```sh
<IfModule mod_ssl.c>
 <VirtualHost *:80>
        ServerName 127.0.0.1
        ServerAdmin info@saudigitus.org

        RewriteEngine On
        <Location /.well-known/acme-challenge>
          Require all denied
          Require host letsencrypt.org
       </Location>
       RewriteRule !^/.well-known/acme-challenge/ https://%{HTTP_HOST} [L,R=permanent]
 </VirtualHost>

 <VirtualHost *:443>
    ServerAdmin info@abc.org
    ServerName 127.0.0.1

    RewriteEngine On

    # RewriteRule   ^/$  /prod/  [R]

    <Proxy *>
        Order deny,allow
        Allow from all
    </Proxy>

    SSLEngine on
    SSLProxyEngine On
    SSLCertificateFile /etc/apache2/ssl/dhis.crt
    SSLCertificateKeyFile /etc/apache2/ssl/dhis.key

    ProxyRequests Off
    ProxyPreserveHost On
    IncludeOptional upstream/*

 </VirtualHost>
</IfModule>
```
3. Activação do ficheiro
```sh
sudo a2ensite localhost.conf
sudo a2dissite 000-default.conf
sudo service apache2 reload
```
Neste momomento, já é possível aceder à página do apache usando o `https://<seu-ip>`

## Criação e instalação instâncias do DHIS2

1. Criação da instância
   Para criar uma instância, é necessário especificar o nome da instância, o IP e o nome do container de base de dados (isso é necessário é possível pode mais de um contêiner do base de dados). Por exemplo, para criar uma instância chamada training:

`sudo dhis2-create-instance training 192.168.0.10 postgres`

2. Deploy do ficheiro .war do DHIS2 na instância criada

`sudo dhis2-deploy-war -l <link-do-war-file> training`

## Alguns comandos úteis

Visualização dos logs:
```sh
sudo dhis2-logview prod
```

Acesso à base de dados:
```sh
lxc exec postgres bash
psql prod
```
Eliminar instância:
```sh
sudo dhis2-delete-instance prod postgres
```
