# NGINX

## Instalação do Nginx

```sh
sudo apt update
```

```sh
sudo apt install nginx
```
Colocando o seu endereço IP  no browser (`<server-ip>`), será possível visualizar a página default do nginx.

Continuando, crie o ficheiro de configuração.
```sh
cd /etc/nginx/sites-available
sudo nano localhost.conf
```

No ficheiro criado, adicione as seguintes configurações:

```properties
server {
	listen 80;
	client_max_body_size 10M;
	# Proxy pass to servlet container
	location / {
		proxy_pass http://localhost:8080/;
		proxy_redirect off;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto http;
		proxy_buffer_size 128k;
		proxy_buffers 8 128k;
		proxy_busy_buffers_size 256k;
	}
}
```

Agora, salve o ficheiro e aActive do ficheiro recentemente criado

```sh
ln -s /etc/nginx/sites-available/localhost.conf /etc/nginx/sites-enabled/
rm /etc/nginx/sites-enabled/default
```

Faça o restart do nginx, usando o comando abaixo:
```sh
sudo service nginx restart
```


Em vez de `<server-ip>:8080`, tente aceder sua instância usando apenas `<server-ip>` simplesmente.

## Certificado SSL

1. Instalação das bibliotecas que fazem a gestão do certificado self-signed

```
sudo apt-get update 
sudo apt-get install openssl
```

2. Criação (gerar) o certificado
   Criando o directório `sudo mkdir /etc/nginx/ssl`

```
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/dhis.key -out /etc/nginx/ssl/dhis.crt
```
No prompt, preencha os dados mediante a solicitação.

Actualize o ficheiro de configuração pelo seguinte:

```properties
server {
    listen 80;
    server_name localhost;
    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate /etc/nginx/ssl/dhis.crt;
    ssl_certificate_key /etc/nginx/ssl/dhis.key;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;

    client_max_body_size 10M;

    # Proxy pass to servlet container
    location / {
        proxy_pass http://localhost:8080;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_buffer_size 128k;
        proxy_buffers 8 128k;
        proxy_busy_buffers_size 256k;
    }
}
```

Faça o restart do nginx, usando o comando abaixo:
```sh
sudo service nginx restart
```

Agora que a aplicação está sendo executada na porta `:443` e/ou `:80`, activaremos o firewall e vamos garantir que esta seja a única porta de entrada.

```
sudo ufw allow http
sudo ufw allow https
sudo ufw enable
```

Neste ponto, não será mais possível aceder à applicação usando a porta `:8080`
>NOTA:
>> Cuidado ao habilitar firewall, pois se não tiver definido correctamente a porta de acesso SSH, poderá perder o acesso ao servidor.


# APACHE2

## Reversy proxy usando o Apache2
> Este método fica como TPC